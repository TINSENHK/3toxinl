<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::all();
        return view('welcome',compact('products'));
    }

    public function show($product)
    {
        $product = Product::where('id',$product)->get();


        return view('productDetail',[
          'products'=>$product
        ]);

        //return view('productDetail',compact('product'));
    }

    public function store()
    {

        $data = request()->validate([
          "product_name" => "required",
          "product_brand" => "required",
          "product_price" => "required",
          "product_discount" => "required",
          "inventory" => "required",
          "product_size" => "required",
          "product_type" => "required",
          "img" => "required|image",
        ]);
        request('img')->store('img/Product/'.request('brand'),'public');


        //return view('productDetail',compact('product'));
    }

}
