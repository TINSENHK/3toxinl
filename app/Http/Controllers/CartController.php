<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Carts;

class CartController extends Controller
{
  public function store()
  {
    $data = request()->validate([
      'product_id' => 'required',
      'quantity' => 'required|integer|between:1,10',
    ]);

    auth()->user()->carts()->create($data);
    return back();
    //auth()->user()->carts();
  }
}
