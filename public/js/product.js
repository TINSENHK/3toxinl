$(document).ready(function () {
    $url = "./Service/ServiceSide.php/Share/";

    product_code = getUrlVars()["product_code"];

    $.getJSON($url + "detail/"+product_code, function (result) {

        console.log(result);
        var gridWrapper = document.querySelector('.content');

  			gridWrapper.innerHTML = '';
  			classie.add(gridWrapper, 'content--loading');

  			setTimeout(function(result) {
  				classie.remove(gridWrapper, 'content--loading');
          html = '<ul class="products">';
          for (i = 0; i < result.length; i++) {
            html += '<li class="product">';
            html += '<div class="producticon">';
            html += '<a href=\"./productDetail.html?product_code='+result[i].product_code+'\">';
            html += "<img class=\"productimg\"src=\"./img/Product/"+result[i].img+"\" width=\"180\" height=\"180\">";
            html += '</a></div>';
          }
          html += '<ul>'
  				gridWrapper.innerHTML = html;
  			}.bind(null,result), 700);


    })
});

function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}
