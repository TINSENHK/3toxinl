$(document).ready(function () {
  /* wait for images to load */
  var menuEl = document.getElementById('ml-menu'),
    mlmenu = new MLMenu(menuEl, {
      // breadcrumbsCtrl : true, // show breadcrumbs
      // initialBreadcrumb : 'all', // initial breadcrumb text
      backCtrl : false, // show back button
      // itemsDelayInterval : 60, // delay between each menu item sliding animation
      onItemClick: loadDummyData // callback: item that doesn´t have a submenu gets clicked - onItemClick([event], [inner HTML of the clicked item])
    });

  // mobile menu toggle
  var openMenuCtrl = document.querySelector('.action--open'),
    closeMenuCtrl = document.querySelector('.action--close');

  openMenuCtrl.addEventListener('click', openMenu);
  closeMenuCtrl.addEventListener('click', closeMenu);

  function openMenu() {
    classie.add(menuEl, 'menu--open');
    closeMenuCtrl.focus();
  }

  function closeMenu() {
    classie.remove(menuEl, 'menu--open');
    openMenuCtrl.focus();
  }

  // simulate grid container loading
  var gridWrapper = document.querySelector('.content');

  function loadDummyData(ev, itemName) {
    ev.preventDefault();

    closeMenu();
    gridWrapper.innerHTML = '';
    classie.add(gridWrapper, 'content--loading');
    setTimeout(function() {
      classie.remove(gridWrapper, 'content--loading');
      //gridWrapper.innerHTML = '<ul class="products">' + dummyData[itemName] + '<ul>';
    }, 700);
  }

  // Get the modal
  var modal = document.getElementById("myModal");

  // Get the button that opens the modal
  var btn = document.getElementsByClassName("openCart");


  // Get the <span> element that closes the modal
  var span = document.getElementsByClassName("close")[0];


  // When the user clicks the button, open the modal
  btn.onclick = function() {
    modal.style.display = "block";
  }

  // When the user clicks on <span> (x), close the modal
  span.onclick = function() {
    modal.style.display = "none";
  }

  // When the user clicks anywhere outside of the modal, close it
  window.onclick = function(event) {
    if (event.target == modal) {
      modal.style.display = "none";
    }
  }

  $('#plus').click(function(){
    var max = 10;
    var oldValue = parseFloat($('#quantity').val());
    if (oldValue >= max) {
      var newVal = oldValue;
    } else {
      var newVal = oldValue + 1;
    }

    $('#quantity').val(newVal);

  });

  $('#minus').click(function() {
    var min = 1;
    var oldValue = parseFloat($('#quantity').val());
    if (oldValue <= min) {
      var newVal = oldValue;
    } else {
      var newVal = oldValue - 1;
    }
    $('#quantity').val(newVal);
  });
});

function showthis(imgs) {
  console.log(imgs.src);
  var expandImg = document.getElementById("expandedImg");
  expandImg.src = imgs.src;
  expandImg.parentElement.style.display = "block";
}

function changeCart(product,url){
  var modal = document.getElementById("myModal");

  modal.style.display = "block";
  console.log(url);
  var mpd = document.getElementById("modalProductDetail");
  var mpi = document.getElementById("modalProduct-img");

  imgs = JSON.parse(product.img);

  html = '<input type="hidden" name="id" value="'+product.id+'">';

  html += '<div class="cart-product-img">';
  html += '<div class="cart-img-container" >';
  html += '<img id="expandedImg" class="product-detail-img" src="'+url + '/' + product.product_brand + '/' + imgs[0] +'">';
  html += '</div>';
  html += '<div class="img-row d-flex flex-row scrollbar" id="style-10">';
  for (var img in imgs) {
    html += '<div class="img-column">';
    assetpath = url + '/' + product.product_brand + '/' + imgs[img];
    html += '<img class="product-img2" src="' + assetpath + '" alt="" onclick="showthis(this);">';
    html += '</div>';
  }
  html += '</div>';
  html += '</div>';
  mpi.innerHTML = html;

  html = '<div class="cart-conform d-flex flex-column p-4" >';
  html += '<H3 class="productName">'+product.product_name+'</H3>';
  html += '<H3 class="productName">'+product.product_brand+'</H3>';

  if (product.product_discount!=null){
      html += '<p class="productOriPrice"><strike>'+product.product_price+' HKD</strike></p>';
      html += '<H3 class="productPrice">'+product.product_price * (product.product_discount/100)+' HKD</H3>';
  }else{
    html += '<H3 class="productPrice">'+product.product_price+' HKD</H3>';
  }
  html += '</div>';

  mpd.innerHTML = html;
}
