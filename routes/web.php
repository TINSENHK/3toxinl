<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();
Route::get('/', 'ProductController@index');

Route::get('home', 'ProductController@index')->name('home.index');


Route::get('product', 'ProductController@index');
Route::get('product/{id}', 'ProductController@show')->name('product.show');
Route::post('product', 'ProductController@store')->name('product.store');


Route::middleware(['auth'])->group(function () {
  Route::post('cart', 'CartController@store')->name('cart.store');
});

Route::middleware(['admin'])->group(function () {
  Route::get('dashboard', function(){
    return view('admin.dashboard');
  });
});
