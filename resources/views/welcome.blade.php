@extends('layouts.app')

@section('content')
        <ul class="product-ul d-flex flex-wrap">

        @forelse($products as $product)

          <li class="product-li">
            @php
              echo '<div class="product-div d-flex flex-column justify-content-between" onclick="location.href=\''. route('product.show',['id' => $product -> id]) . '\'">'
            @endphp
                <div class="producticon">
                    @php
                        $imgs = json_encode($product->img);
                        $imgs = json_decode(json_decode($imgs),true);
                        echo '<img class="product-img" src="' . asset('storage/img/Product/' . $product -> product_brand . '/' . $imgs[0]).'">';
                    @endphp
                </div>
                <div class="product-sale-icon"><span class="sale">{{100 - $product -> product_discount}}%</span></div>
                <div class="overlay">
                </div>
                <div class="product-info d-flex flex-column justify-content-between">
                    @php
                        $price = $product -> product_price;
                        $discount_price = $price * ($product -> product_discount / 100);
                    @endphp
                    <strong>{{ $product-> product_name }}</strong>
                    @if (empty($product -> product_discount))
                        <strong>HK$ {{ $price }}</strong>
                    @else
                    <div class="">
                        <strong>HK$ <strike class="product-discount-price">{{ $price }}</strike></strong> <br>
                        <strong>HK$ {{ $discount_price }} </strong>
                    </div>
                    @endif
                </div>
            </div>

            <a class="product-cart" href="#" onclick='changeCart({{$product}},{{"\"". asset("storage/img/Product/") ."\""}})'> Add cart </a>

        </li>
    @empty
        <p>No product</p>
    @endforelse

    </ul>
      <div id="myModal" class="modal">
        <div class="modal-content">
          <span class="close align-self-end">&times;</span>
          <form method="POST" action="{{ route('cart.store') }}">
            @csrf
            <div class="d-flex justify-content-between">
                <div id='modalProduct-img' class="modal-product-img"></div>
                <div class="modal-product-detail">
                  <div id='modalProductDetail'></div>
                    <div class="p-2 d-flex align-items-end flex-column">
                      <div class="input-group mb-3 w-75">
                        <div class="input-group-prepend">
                          <button id="plus" class="btn btn-outline-secondary" type="button">+</button>
                        </div>
                        <input
                          id="quantity"
                          type="number"
                          class="form-control text-right"
                          value="11"
                          name="quantity">
                        <div class="input-group-append">
                          <button id="minus" class="btn btn-outline-secondary" type="button">-</button>
                        </div>
                      </div>
                      <div class="d-flex justify-content-center">
                        <input class="btn text-light" type="submit" value="Add">
                      </div>
                    </div>
                  </div>
              </div>
          </form>
        </div>

      </div>

@endsection
