@extends('layouts.app')

@section('content')
    @forelse ($products as $product)


    <div class="img">
      <?php
        $imgs = json_encode($product->img);
        $imgs = json_decode(json_decode($imgs),true);
        $html = '<div class="img-container">';
        $html .= '<img id="expandedImg" class="product-detail-img" src="'. asset('storage/img/Product/' . $product -> product_brand . '/' . $imgs[0]).'">';
        $html .= '<div id="imgtext"></div>';
        $html .= '</div>';
        $html .= '<div class="img-row d-flex flex-row scrollbar" id="style-10">';
        foreach ($imgs as $img => $value) {
          $html .= '<div class="img-column">';
          $assetpath = asset('storage/img/Product/' . $product -> product_brand . '/' . $value);
          $html .= '<img class="product-img2" src="' . $assetpath . '" alt="" onclick="showthis(this);">';
          $html .= '</div>';
        }

        print_r($html);
        // {{$product -> product_name}}
        // {{$product -> product_brand}}
        // {{$product -> product_price}}
        // {{$product -> product_brand}}
        // {{$product -> product_discount}}
        // {{$product -> product_discount}}
        //
        // {{$product -> inventory}}
      ?>
      </div>
    </div>

      <p>
        {
          "id":1,
          "product_name":"a",
          "product_brand":"b",
          "product_price":"369.00",
          "product_discount":20,
          "inventory":4,
          "product_code":"b123456",
          "product_size":"42",
          "product_type":"shoes",
          "product_sales":5,
          "img":"{\"0\": \"0.jpg\", \"1\": \"1.jpg\"}",
          "created_at":"2020-03-02 11:59:53",
          "updated_at":"2020-03-02 11:59:53"}
      </p>
    @empty
        <p class='text-center'>page not find</p>
    @endforelse
@endsection
