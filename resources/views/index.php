<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>3ToxinL</title>
  <!-- food icons -->
	<link rel="stylesheet" type="text/css" href="../MultiLevelMenu-master/css/component.css" />
	<!-- menu styles -->
	<link rel="stylesheet" type="text/css" href="./style.css" />
  <link rel="stylesheet" href="../MultiLevelMenu-master/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
  <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  <script src="../MultiLevelMenu-master/js/modernizr-custom.js"></script>
  <script src="../MultiLevelMenu-master/js/classie.js"></script>
  <script src="../MultiLevelMenu-master/js/main.js"></script>
  <script src="../js/init.js"></script>
  <script src="../js/ready.js"></script>

  </head>

  <body>
  	<!-- Main container -->
  	<div class="container">
  		<!-- Blueprint header -->
  		<header class="bp-header cf">
  			<div class="dummy-logo">
  				<div class="dummy-icon foodicon foodicon--coconut"></div>
  				<h2 class="dummy-heading">Fooganic</h2>
  			</div>
  			<div class="bp-header__main">
          <a class="bp-header__present to-pointer" onclick="goLogin()">Log in</a>
          <a class="bp-header__present to-pointer" onclick="goSignup()">Sign Up</a><br />

  				<a class="link" aria-hidden="true" href="http://tympanus.net/Blueprints/PageStackNavigation/" data-info="previous Blueprint"><i class="fa fa-instagram" style="color:#5c5edc"></i><span class="tooltiptext">Check our Instagram Shop</span></a>
  				<a class="link" aria-hidden="true" href="http://tympanus.net/codrops/?p=25521" data-info="back to the Codrops article"><i class="fa fa-facebook" style="color:#5c5edc"></i><span class="tooltiptext">Go Facebook</span></a>
          <p class></p>
  			</div>
  		</header>
  		<button class="action action--open" aria-label="Open Menu"><span class="icon icon--menu"></span></button>
  		<nav id="ml-menu" class="menu">
  			<button class="action action--close" aria-label="Close Menu"><span class="icon icon--cross" style="color:White"></span></button>
  			<div class="menu__wrap">
  				<ul data-menu="main" class="menu__level" tabindex="-1" role="menu" aria-label="All">
  					<li class="menu__item" role="menuitem"><a class="menu__link" href="" onclick="window.location.href=this">Home</a></li>
  					<li class="menu__item" role="menuitem"><a class="menu__link" data-submenu="submenu-2" aria-owns="submenu-2" href="#">Fruits</a></li>
  					<li class="menu__item" role="menuitem"><a class="menu__link" data-submenu="submenu-3" aria-owns="submenu-3" href="#">Grains</a></li>
  					<li class="menu__item" role="menuitem"><a class="menu__link" data-submenu="submenu-4" aria-owns="submenu-4" href="#">Mylk &amp; Drinks</a></li>
  				</ul>
  				<!-- Submenu 2 -->
  				<ul data-menu="submenu-2" id="submenu-2" class="menu__level" tabindex="-1" role="menu" aria-label="Fruits">
  					<li class="menu__item" role="menuitem"><a class="menu__link" href="#">Citrus Fruits</a></li>
  					<li class="menu__item" role="menuitem"><a class="menu__link" href="#">Berries</a></li>
  					<li class="menu__item" role="menuitem"><a class="menu__link" data-submenu="submenu-2-1" aria-owns="submenu-2-1" href="#">Special Selection</a></li>
  					<li class="menu__item" role="menuitem"><a class="menu__link" href="#">Tropical Fruits</a></li>
  					<li class="menu__item" role="menuitem"><a class="menu__link" href="#">Melons</a></li>
  				</ul>
  				<!-- Submenu 2-1 -->
  				<ul data-menu="submenu-2-1" id="submenu-2-1" class="menu__level" tabindex="-1" role="menu" aria-label="Special Selection">
  					<li class="menu__item" role="menuitem"><a class="menu__link" href="#">Exotic Mixes</a></li>
  					<li class="menu__item" role="menuitem"><a class="menu__link" href="#">Wild Pick</a></li>
  					<li class="menu__item" role="menuitem"><a class="menu__link" href="#">Vitamin Boosters</a></li>
  				</ul>
  				<!-- Submenu 3 -->
  				<ul data-menu="submenu-3" id="submenu-3" class="menu__level" tabindex="-1" role="menu" aria-label="Grains">
  					<li class="menu__item" role="menuitem"><a class="menu__link" href="#">Buckwheat</a></li>
  					<li class="menu__item" role="menuitem"><a class="menu__link" href="#">Millet</a></li>
  					<li class="menu__item" role="menuitem"><a class="menu__link" href="#">Quinoa</a></li>
  					<li class="menu__item" role="menuitem"><a class="menu__link" href="#">Wild Rice</a></li>
  					<li class="menu__item" role="menuitem"><a class="menu__link" href="#">Durum Wheat</a></li>
  					<li class="menu__item" role="menuitem"><a class="menu__link" data-submenu="submenu-3-1" aria-owns="submenu-3-1" href="#">Promo Packs</a></li>
  				</ul>
  				<!-- Submenu 3-1 -->
  				<ul data-menu="submenu-3-1" id="submenu-3-1" class="menu__level" tabindex="-1" role="menu" aria-label="Promo Packs">
  					<li class="menu__item" role="menuitem"><a class="menu__link" href="#">Starter Kit</a></li>
  					<li class="menu__item" role="menuitem"><a class="menu__link" href="#">The Essential 8</a></li>
  					<li class="menu__item" role="menuitem"><a class="menu__link" href="#">Bolivian Secrets</a></li>
  					<li class="menu__item" role="menuitem"><a class="menu__link" href="#">Flour Packs</a></li>
  				</ul>
  				<!-- Submenu 4 -->
  				<ul data-menu="submenu-4" id="submenu-4" class="menu__level" tabindex="-1" role="menu" aria-label="Mylk &amp; Drinks">
  					<li class="menu__item" role="menuitem"><a class="menu__link" href="#">Grain Mylks</a></li>
  					<li class="menu__item" role="menuitem"><a class="menu__link" href="#">Seed Mylks</a></li>
  					<li class="menu__item" role="menuitem"><a class="menu__link" href="#">Nut Mylks</a></li>
  					<li class="menu__item" role="menuitem"><a class="menu__link" href="#">Nutri Drinks</a></li>
  					<li class="menu__item" role="menuitem"><a class="menu__link" data-submenu="submenu-4-1" aria-owns="submenu-4-1" href="#">Selection</a></li>
  				</ul>
  				<!-- Submenu 4-1 -->
  				<ul data-menu="submenu-4-1" id="submenu-4-1" class="menu__level" tabindex="-1" role="menu" aria-label="Selection">
  					<li class="menu__item" role="menuitem"><a class="menu__link" href="#">Nut Mylk Packs</a></li>
  					<li class="menu__item" role="menuitem"><a class="menu__link" href="#">Amino Acid Heaven</a></li>
  					<li class="menu__item" role="menuitem"><a class="menu__link" href="#">Allergy Free</a></li>
  				</ul>
  			</div>
  		</nav>
  		<div class="content">
  			<p class="info">Please choose a category</p>
  			<!-- Ajax loaded content here -->
  		</div>
  	</div>
  	<!-- /view -->

  </body>

  </html>
