@extends('layouts.app')
@section('content')
<div class="container">
  <form class="" action="{{route('product.store')}}" method="post" enctype="multipart/form-data">
    @csrf
    <div class="input-group mb-3">
      <div class="input-group-prepend w-25">
        <span class="input-group-text w-100">product name</span>
      </div>
      <input type="text" class="form-control @error('product_name') is-invalid @enderror" id="product_name" name="product_name">
      @error('product_name')
          <span class="invalid-feedback" role="alert">
              <strong>{{ $message }}</strong>
          </span>
      @enderror
    </div>
    <div class="input-group mb-3">
      <div class="input-group-prepend w-25">
        <span class="input-group-text w-100">brand</span>
      </div>
      <input type="text" class="form-control @error('product_brand') is-invalid @enderror" id="product_brand" name="product_brand">
      @error('product_brand')
          <span class="invalid-feedback" role="alert">
              <strong>{{ $message }}</strong>
          </span>
      @enderror
    </div>
    <div class="input-group mb-3">
      <div class="input-group-prepend w-25">
        <span class="input-group-text w-100">product price</span>
      </div>
      <input type="text" class="form-control @error('product_price') is-invalid @enderror" id="product_price" name="product_price">
      @error('product_price')
          <span class="invalid-feedback" role="alert">
              <strong>{{ $message }}</strong>
          </span>
      @enderror
    </div>
    <div class="input-group mb-3">
      <div class="input-group-prepend w-25">
        <span class="input-group-text w-100">product discount</span>
      </div>
      <input type="text" class="form-control @error('product_discount') is-invalid @enderror" id="product_discount" name="product_discount">
      @error('product_discount')
          <span class="invalid-feedback" role="alert">
              <strong>{{ $message }}</strong>
          </span>
      @enderror
    </div>
    <div class="input-group mb-3">
      <div class="input-group-prepend w-25">
        <span class="input-group-text w-100">inventory</span>
      </div>
      <input type="text" class="form-control @error('inventory') is-invalid @enderror" id="inventory" name="inventory">
      @error('inventory')
          <span class="invalid-feedback" role="alert">
              <strong>{{ $message }}</strong>
          </span>
      @enderror
    </div>
    <div class="input-group mb-3">
      <div class="input-group-prepend w-25">
        <span class="input-group-text w-100">product code</span>
      </div>
      <input type="text" class="form-control @error('product_code') is-invalid @enderror" id="product_code" name="product_code">
      @error('product_code')
          <span class="invalid-feedback" role="alert">
              <strong>{{ $message }}</strong>
          </span>
      @enderror
    </div>
    <div class="input-group mb-3">
      <div class="input-group-prepend w-25">
        <span class="input-group-text w-100">product type</span>
      </div>
      <input type="text" class="form-control @error('product_type') is-invalid @enderror" id="product_type" name="product_type">
      @error('product_type')
          <span class="invalid-feedback" role="alert">
              <strong>{{ $message }}</strong>
          </span>
      @enderror
    </div>
    <div class="input-group mb-3">
      <div class="input-group-prepend w-25">
        <span class="input-group-text w-100">img</span>
      </div>
      <input type="file" class="form-control @error('img') is-invalid @enderror" id="img" name="img">
      @error('img')
          <span class="invalid-feedback" role="alert">
              <strong>{{ $message }}</strong>
          </span>
      @enderror
    </div>
    <input type="submit" name="" value="add">
</form>

</div>
@endsection
