<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- jquery -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"
            integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
            integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
            crossorigin="anonymous"></script>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- custom -->

    <script src="{{ asset('MultiLevelMenu-master/js/modernizr-custom.js') }}"></script>
    <script src="{{ asset('MultiLevelMenu-master/js/classie.js') }}"></script>
    <script src="{{ asset('MultiLevelMenu-master/js/main.js') }}"></script>
    <script src="{{ asset('js/init.js') }}"></script>
    <link rel="stylesheet"
          href="{{ asset('MultiLevelMenu-master/fonts/font-awesome-4.7.0/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('MultiLevelMenu-master/css/component.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/style.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/style_org.css') }}"/>
</head>
<body>
<div id="app">
    <a class="" href="{{ url('/') }}">
        <img src="{{ asset('storage/img/3ToxinL_.jpeg') }}" alt="" width="300px">
    </a>
    <div class="container-fluid">
        <button class="action action--open" aria-label="Open Menu"><span class="icon icon--menu"></span></button>

        <nav id="ml-menu" class="menu">

            <button class="action action--close" aria-label="Close Menu"><span class="icon icon--cross"
                                                                               style="color:White"></span></button>
            <div class="menu__wrap">

                <ul data-menu="main" class="menu__level" tabindex="-1" role="menu" aria-label="All">
                    <li class="menu__item" role="menuitem">
                        <a class="menu__link" href="#" onclick="window.location.href='{{ route('home.index') }}'"
                           aria-owns="submenu-1">主頁</a>
                        <a class="menu__link" href="#" onclick="window.location.href='{{ route('home.index') }}'"
                           aria-owns="submenu-1">限時優惠</a>
                        <a class="menu__link" href="#" onclick="window.location.href='{{ route('home.index') }}'"
                           aria-owns="submenu-1">預訂產品</a>
                        <a class="menu__link" href="#" onclick="window.location.href='{{ route('home.index') }}'"
                           aria-owns="submenu-1">所有產品</a>
                    </li>
                    <li class="menu__item" role="menuitem"><a class="menu__link" data-submenu="submenu-1"
                                                              aria-owns="submenu-1" href="#">品牌</a></li>
                </ul>
                <!-- Submenu 1 -->
                <ul data-menu="submenu-1" id="submenu-1" class="menu__level" tabindex="-1" role="menu"
                    aria-label="Mylk &amp; Drinks">
                    <li class="menu__item" role="menuitem"><a class="menu__link" href="#">Adidas</a></li>
                    <li class="menu__item" role="menuitem"><a class="menu__link" href="#">Converse</a></li>
                    <li class="menu__item" role="menuitem"><a class="menu__link" href="#">Champion</a></li>
                    <li class="menu__item" role="menuitem"><a class="menu__link" href="#">Dickies</a></li>
                    <li class="menu__item" role="menuitem"><a class="menu__link" href="#">Hollister</a></li>
                    <li class="menu__item" role="menuitem"><a class="menu__link" href="#">Nike</a></li>
                    <li class="menu__item" role="menuitem"><a class="menu__link" href="#">New Balance</a></li>
                    <li class="menu__item" role="menuitem"><a class="menu__link" href="#">Reebok</a></li>
                    <li class="menu__item" role="menuitem"><a class="menu__link" href="#">RipnDip</a></li>
                    <li class="menu__item" role="menuitem"><a class="menu__link" href="#">The North Face</a></li>
                    <li class="menu__item" role="menuitem"><a class="menu__link" href="#">Puma</a></li>
                    <li class="menu__item" role="menuitem"><a class="menu__link" href="#">Y-3</a></li>
                    <li class="menu__item" role="menuitem"><a class="menu__link" href="#">Vans</a></li>
                </ul>
            </div>

        </nav>
        <div class="content" style="background-color:#2a2b30">

            <div class="bp-header__main">
                @guest

                    <a class="link" href="{{ route('login') }}">登入 <i class="fa fa-sign-in"></i></a>

                    @if (Route::has('register'))
                        <a class="link" href="{{ route('register') }}">加入會員 <i class="fa fa-user-plus"></i></a>
                    @endif
                @else

                    <a class="link" href="#" role="button">
                        購物車 <i class="fa fa-shopping-cart"></i>
                    </a>


                    <a class="link" href="#" role="button">
                        歡迎, {{ Auth::user()->name }} <i class="fa fa-user"></i> <span class="caret"></span>
                    </a>


                    <a class="link" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                        登出 <i class="fa fa-sign-out"></i>
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>

                @endguest
            </div>
            @yield('content')
        </div>
    </div>
</div>

</body>
</html>
