<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_order', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('customer_id')->unique();
            $table->string('payment_method');
            $table->string('paid_state');
            $table->decimal('delivery_status', 8, 2);
            $table->string('delivery_address')->nullable();
            $table->decimal('total_price', 8, 2);
            $table->string('order_remark')->nullable();
            $table->json('payment_img')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_order');
    }
}
